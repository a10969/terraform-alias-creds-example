resource "google_service_account" "project_owner" {
  account_id = "owner-sa"
  project    = google_project.project.project_id
}

resource "google_service_account_key" "project_owner_key" {
  service_account_id = google_service_account.project_owner.name
}

resource "google_project_iam_member" "owner" {
  project = google_project.project.project_id
  member  = "serviceAccount:${google_service_account.project_owner.email}"
  role    = "roles/owner"
}