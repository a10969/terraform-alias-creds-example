resource "google_project_service" "bigquery" {
  project                    = google_project.project.project_id
  service                    = "bigquery.googleapis.com"
  disable_dependent_services = true
}
