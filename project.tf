resource "google_project" "project" {
  name       = "TF alias ${random_string.suffix.result}"
  project_id = "tf-alias-${random_string.suffix.result}"
  folder_id  = var.folder_id
}
