terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.20.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.1.3"
    }
  }
}

provider "google" {
  # Configuration options
  credentials = "key.json"
}

provider "google" {
  alias       = "project"
  project     = google_project.project.project_id
  credentials = base64decode(google_service_account_key.project_owner_key.private_key)
}
