# With alias

This branch covers the case where we use an alias

## Prerequisites
Before starting, we assume you have a Google Cloud Project (`ops`) hosting a service account with rights
to create projects in a specific folder
* Google Cloud Project (`ops`)
* Service account in `ops` with `project.creator` role in a specific folder
* Enable IAM api in `ops`

## Terraform setup
![alias](images/alias.svg)
We initialize the Terraform provider using the existing service account. Because the Terraform code
is defined to automatically use an alias provider for `p1` resources, the code will succeed
## Run the code sample
To run this code sample
1. download the key of the service account from your `ops` project and save it at the root of the directory under `key.json`
2. Run `terraform apply`
3. Enter the folder ID (e.g `70896136027`) where to create the project `p1`

The plan succeeds
```shell
Apply complete! Resources: 7 added, 0 changed, 0 destroyed.
```
