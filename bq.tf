resource "google_bigquery_dataset" "dataset" {
  provider      = google.project
  dataset_id    = "example_dataset"
  friendly_name = "test"
  description   = "This is a test description"
  location      = "EU"
  depends_on    = [google_project_service.bigquery]
}
